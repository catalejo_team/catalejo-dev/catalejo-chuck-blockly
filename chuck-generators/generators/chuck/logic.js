/**
 * @license
 * Copyright 2014 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Generating Chuck for logic blocks.
 */
'use strict';

goog.module('Blockly.Chuck.logic');

const {chuckGenerator: Chuck} = goog.require('Blockly.Chuck');


Chuck['controls_if'] = function(block) {
  // If/elseif/else condition.
  let n = 0;
  let code = '', branchCode, conditionCode;
  if (Chuck.STATEMENT_PREFIX) {
    // Automatic prefix insertion is switched off for this block.  Add manually.
    code += Chuck.injectId(Chuck.STATEMENT_PREFIX, block);
  }
  do {
    conditionCode =
        Chuck.valueToCode(block, 'IF' + n, Chuck.ORDER_NONE) || 'false';
    branchCode = Chuck.statementToCode(block, 'DO' + n);
    if (Chuck.STATEMENT_SUFFIX) {
      branchCode =
          Chuck.prefixLines(
              Chuck.injectId(Chuck.STATEMENT_SUFFIX, block), Chuck.INDENT) +
          branchCode;
    }
    code += (n > 0 ? 'else ' : '') + 'if (' + conditionCode + ') {\n' +
        branchCode + '}';
    n++;
  } while (block.getInput('IF' + n));

  if (block.getInput('ELSE') || Chuck.STATEMENT_SUFFIX) {
    branchCode = Chuck.statementToCode(block, 'ELSE');
    if (Chuck.STATEMENT_SUFFIX) {
      branchCode =
          Chuck.prefixLines(
              Chuck.injectId(Chuck.STATEMENT_SUFFIX, block), Chuck.INDENT) +
          branchCode;
    }
    code += ' else {\n' + branchCode + '}';
  }
  return code + '\n';
};

Chuck['controls_ifelse'] = Chuck['controls_if'];

Chuck['logic_compare'] = function(block) {
  // Comparison operator.
  const OPERATORS =
      {'EQ': '==', 'NEQ': '!=', 'LT': '<', 'LTE': '<=', 'GT': '>', 'GTE': '>='};
  const operator = OPERATORS[block.getFieldValue('OP')];
  const order = (operator === '==' || operator === '!=') ?
      Chuck.ORDER_EQUALITY :
      Chuck.ORDER_RELATIONAL;
  const argument0 = Chuck.valueToCode(block, 'A', order) || '0';
  const argument1 = Chuck.valueToCode(block, 'B', order) || '0';
  const code = argument0 + ' ' + operator + ' ' + argument1;
  return [code, order];
};

Chuck['logic_operation'] = function(block) {
  // Operations 'and', 'or'.
  const operator = (block.getFieldValue('OP') === 'AND') ? '&&' : '||';
  const order =
      (operator === '&&') ? Chuck.ORDER_LOGICAL_AND : Chuck.ORDER_LOGICAL_OR;
  let argument0 = Chuck.valueToCode(block, 'A', order);
  let argument1 = Chuck.valueToCode(block, 'B', order);
  if (!argument0 && !argument1) {
    // If there are no arguments, then the return value is false.
    argument0 = 'false';
    argument1 = 'false';
  } else {
    // Single missing arguments have no effect on the return value.
    const defaultArgument = (operator === '&&') ? 'true' : 'false';
    if (!argument0) {
      argument0 = defaultArgument;
    }
    if (!argument1) {
      argument1 = defaultArgument;
    }
  }
  const code = argument0 + ' ' + operator + ' ' + argument1;
  return [code, order];
};

Chuck['logic_negate'] = function(block) {
  // Negation.
  const order = Chuck.ORDER_UNARY_PREFIX;
  const argument0 = Chuck.valueToCode(block, 'BOOL', order) || 'true';
  const code = '!' + argument0;
  return [code, order];
};

Chuck['logic_boolean'] = function(block) {
  // Boolean values true and false.
  const code = (block.getFieldValue('BOOL') === 'TRUE') ? 'true' : 'false';
  return [code, Chuck.ORDER_ATOMIC];
};

Chuck['logic_null'] = function(block) {
  // Null data type.
  return ['null', Chuck.ORDER_ATOMIC];
};

Chuck['logic_ternary'] = function(block) {
  // Ternary operator.
  const value_if =
      Chuck.valueToCode(block, 'IF', Chuck.ORDER_CONDITIONAL) || 'false';
  const value_then =
      Chuck.valueToCode(block, 'THEN', Chuck.ORDER_CONDITIONAL) || 'null';
  const value_else =
      Chuck.valueToCode(block, 'ELSE', Chuck.ORDER_CONDITIONAL) || 'null';
  const code = value_if + ' ? ' + value_then + ' : ' + value_else;
  return [code, Chuck.ORDER_CONDITIONAL];
};
