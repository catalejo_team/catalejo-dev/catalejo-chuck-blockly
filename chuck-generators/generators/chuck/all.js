/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Complete helper functions for generating Chuck for
 *     blocks.  This is the entrypoint for dart_compressed.js.
 * @suppress {extraRequire}
 */
'use strict';

goog.module('Blockly.Chuck.all');

const moduleExports = goog.require('Blockly.Chuck');
// goog.require('Blockly.Chuck.colour');
// goog.require('Blockly.Chuck.lists');
goog.require('Blockly.Chuck.logic');
goog.require('Blockly.Chuck.loops');
goog.require('Blockly.Chuck.math');
goog.require('Blockly.Chuck.procedures');
goog.require('Blockly.Chuck.texts');
goog.require('Blockly.Chuck.variables');
goog.require('Blockly.Chuck.variablesDynamic');

exports = moduleExports;
