/**
 * @license
 * Copyright 2014 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Generating Chuck for variable blocks.
 */
'use strict';

goog.module('Blockly.Chuck.variables');

const {NameType} = goog.require('Blockly.Names');
const {chuckGenerator: Chuck} = goog.require('Blockly.Chuck');


Chuck['variables_get'] = function(block) {
  // Variable getter.
  const code =
      Chuck.nameDB_.getName(block.getFieldValue('VAR'), NameType.VARIABLE);
  return [code, Chuck.ORDER_ATOMIC];
};

Chuck['variables_set'] = function(block) {
  // Variable setter.
  const argument0 =
      Chuck.valueToCode(block, 'VALUE', Chuck.ORDER_ASSIGNMENT) || '0';
  const varName =
      Chuck.nameDB_.getName(block.getFieldValue('VAR'), NameType.VARIABLE);
  return argument0 + ' => ' + varName + ';\n';
};
