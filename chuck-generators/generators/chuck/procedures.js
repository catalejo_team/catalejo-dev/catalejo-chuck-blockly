/**
 * @license
 * Copyright 2014 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Generating Chuck for procedure blocks.
 */
'use strict';

goog.module('Blockly.Chuck.procedures');

const {NameType} = goog.require('Blockly.Names');
const {chuckGenerator: Chuck} = goog.require('Blockly.Chuck');


Chuck['procedures_defreturn'] = function(block) {
  // Define a procedure with a return value.
  const funcName =
      Chuck.nameDB_.getName(block.getFieldValue('NAME'), NameType.PROCEDURE);
  let xfix1 = '';
  if (Chuck.STATEMENT_PREFIX) {
    xfix1 += Chuck.injectId(Chuck.STATEMENT_PREFIX, block);
  }
  if (Chuck.STATEMENT_SUFFIX) {
    xfix1 += Chuck.injectId(Chuck.STATEMENT_SUFFIX, block);
  }
  if (xfix1) {
    xfix1 = Chuck.prefixLines(xfix1, Chuck.INDENT);
  }
  let loopTrap = '';
  if (Chuck.INFINITE_LOOP_TRAP) {
    loopTrap = Chuck.prefixLines(
        Chuck.injectId(Chuck.INFINITE_LOOP_TRAP, block), Chuck.INDENT);
  }
  const branch = Chuck.statementToCode(block, 'STACK');
  let returnValue = Chuck.valueToCode(block, 'RETURN', Chuck.ORDER_NONE) || '';
  let xfix2 = '';
  if (branch && returnValue) {
    // After executing the function body, revisit this block for the return.
    xfix2 = xfix1;
  }
  if (returnValue) {
    returnValue = Chuck.INDENT + 'return ' + returnValue + ';\n';
  }
  const returnType = returnValue ? 'dynamic' : 'void';
  const args = [];
  const variables = block.getVars();
  for (let i = 0; i < variables.length; i++) {
    args[i] = Chuck.nameDB_.getName(variables[i], NameType.VARIABLE);
  }
  let code = returnType + ' ' + funcName + '(' + args.join(', ') + ') {\n' +
      xfix1 + loopTrap + branch + xfix2 + returnValue + '}';
  code = Chuck.scrub_(block, code);
  // Add % so as not to collide with helper functions in definitions list.
  Chuck.definitions_['%' + funcName] = code;
  return null;
};

// Defining a procedure without a return value uses the same generator as
// a procedure with a return value.
Chuck['procedures_defnoreturn'] = Chuck['procedures_defreturn'];

Chuck['procedures_callreturn'] = function(block) {
  // Call a procedure with a return value.
  const funcName =
      Chuck.nameDB_.getName(block.getFieldValue('NAME'), NameType.PROCEDURE);
  const args = [];
  const variables = block.getVars();
  for (let i = 0; i < variables.length; i++) {
    args[i] = Chuck.valueToCode(block, 'ARG' + i, Chuck.ORDER_NONE) || 'null';
  }
  let code = funcName + '(' + args.join(', ') + ')';
  return [code, Chuck.ORDER_UNARY_POSTFIX];
};

Chuck['procedures_callnoreturn'] = function(block) {
  // Call a procedure with no return value.
  // Generated code is for a function call as a statement is the same as a
  // function call as a value, with the addition of line ending.
  const tuple = Chuck['procedures_callreturn'](block);
  return tuple[0] + ';\n';
};

Chuck['procedures_ifreturn'] = function(block) {
  // Conditionally return value from a procedure.
  const condition =
      Chuck.valueToCode(block, 'CONDITION', Chuck.ORDER_NONE) || 'false';
  let code = 'if (' + condition + ') {\n';
  if (Chuck.STATEMENT_SUFFIX) {
    // Inject any statement suffix here since the regular one at the end
    // will not get executed if the return is triggered.
    code += Chuck.prefixLines(
        Chuck.injectId(Chuck.STATEMENT_SUFFIX, block), Chuck.INDENT);
  }
  if (block.hasReturnValue_) {
    const value = Chuck.valueToCode(block, 'VALUE', Chuck.ORDER_NONE) || 'null';
    code += Chuck.INDENT + 'return ' + value + ';\n';
  } else {
    code += Chuck.INDENT + 'return;\n';
  }
  code += '}\n';
  return code;
};
