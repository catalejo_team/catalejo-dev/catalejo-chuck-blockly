#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	Iniciar diferentes herramientas para desarrollo de blockly en chuck
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Tuesday 24 January 2023
status=$?

BROWSER=firefox
BLOCKLYFACTORY=../../blockly/demos/blockfactory/index.html
BLOCKLYFACTORY_OLD=../../blockly/demos/blockfactory_old/index.html

$BROWSER ./index.html $BLOCKLYFACTORY $BLOCKLYFACTORY_OLD
