# Blockly ChucK

Creación de bloques para ChucK y generadores de código
de blockly to chuck

## Construcciones

### Compilación de blockly-chuck

Instalación

```bash
cd ./blockly
npm install
```

```bash
cd ./blockly
npm run build:generators
```

## Referencias

[Ejemplos de uso de blockly por codelabs](https://google.github.io/blockly-samples/)

[blockly codelabs](https://blocklycodelabs.dev/)
